#!/usr/bin/env python3

import cv2
import face_recognition
from time import sleep
from playsound import playsound

# Load known faces
known_faces = []
names = ["INPUTNAME"]

for name in names:
    image = face_recognition.load_image_file(f"known_faces/{name}/{name}.jpg")
    encoding = face_recognition.face_encodings(image)[0]
    known_faces.append(encoding)

# Load the default audio file
default_audio = "audio.wav"

# Start the webcam
video_capture = cv2.VideoCapture(0)

while True:
    # Capture each frame
    ret, frame = video_capture.read()

    # Find all face locations in the frame
    face_locations = face_recognition.face_locations(frame)
    face_encodings = face_recognition.face_encodings(frame, face_locations)

    for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
        # Check if the face matches any of the known faces
        matches = face_recognition.compare_faces(known_faces, face_encoding)

        name = "Unknown"
        if True in matches:
            first_match_index = matches.index(True)
            name = names[first_match_index]

        # Play the corresponding audio file
        audio_file = f"{name}.wav" if name != "Unknown" else default_audio
        playsound(audio_file)
        sleep(5)

    # Display the resulting frame
    cv2.imshow("Video", frame)

    # Break the loop with 'q' key
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the webcam and close all windows
video_capture.release()
cv2.destroyAllWindows()
